Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :user_profiles

	resources :messages
  resources :invoices, only: [:new]
  resources :questionnaires, only: [:create]

  resources :charges

  get '/order', to: "invoices#order"

  get "/checkout", to: "static_pages#checkout"

	root "static_pages#home"

	get '/about', to: 'static_pages#about'

	get '/contact', to: 'static_pages#contact'

	get '/faqs', to: 'static_pages#faqs'

	post '/contact', to: 'messages#create'

end