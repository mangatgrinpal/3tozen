class MessagesController < ApplicationController

	def new
		@message = Message.new
	end

	#def show
	#	@message = Message.find(params[:id])
	#end

	def create
		@message = Message.new(message_params)
		if @message.save
			MessagesMailer.send_email(@message).deliver_now!
			flash[:notice] = "Your message has been sent."
			redirect_to contact_path
		else
			flash[:error] = "Sorry, there was an issue sending your message. Try again please."
			redirect_to contact_path
		end
	end

	private
	
		def message_params
			params.require(:messages).permit(:name, :subject, :email, :body)
		end
end