class ChargesController < ApplicationController
  def new
    @user = User.find(session[:user_id])
    @invoice = Invoice.find(session[:invoice_id])
  end

  def create
    # Amount in cents
    @amount = render_amount

    customer = Stripe::Customer.create(
      :email => params[:stripe_email],
      :source  => params[:stripe_token]
    )

    charge = Stripe::Charge.create(
      :customer    => customer.id,
      :amount      => @amount,
      :description => '3toZen Purchase',
      :currency    => 'usd'
    )

  save_stripe_info_to_session
  redirect_to new_charge_path

  rescue Stripe::CardError => e
    flash[:error] = e.message
    redirect_to new_charge_path
  end



  private
  # need to handle user buying multiple items or have dif shipping address/billing addresses

  def save_stripe_info_to_session
    user = User.new(email: params[:stripe_email])
    invoice = user.invoices.new(total: 5000, status: "paid")
    user.build_address(Address.stripe_params_mapping(stripe_params))
    user.build_billing_address(BillingAddress.stripe_params_mapping(stripe_params))

    q = Questionnaire.find(session[:questionnaire_id])
    q.zipcode = params["shipping_address_zip"]
    user.questionnaire = q
    if user.save
      session[:user_id] = user.id
      session[:invoice_id] = invoice.id
    end

    session[:item] = params[:item]
  end

  def stripe_params
    params.permit!
  end

  def render_amount
    if params[:item] == "oneMonth"
      8630
    else
      16060
    end
  end
end
