class QuestionnairesController < ApplicationController
  def create
    if (q = Questionnaire.create(answer_params)).valid?
      session[:questionnaire_id] = q.id
      render json: {redirectPath: checkout_path}
    else
      render status: 400
    end
  end

  def answer_params
    params[:answers].delete(:step)
    params.require(:answers).permit!
  end
end
