class StaticPagesController < ApplicationController

  def home
  end

  def about
  end

  def contact
  	@message = Message.new
  end

  def faqs
  end

  def checkout
  end
end