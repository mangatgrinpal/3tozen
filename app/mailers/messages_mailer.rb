class MessagesMailer < ApplicationMailer
	default from: "noreply@example.com"

	def send_email(message)
		@message = message 
		mail(to: message.email, subject: 'Message received')
	end
end
