class Invoice < ApplicationRecord
  belongs_to :user
  before_save :set_invoice_number

  private

  def set_invoice_number
    self.invoice_number = "#{Time.now.strftime("%Y%M%S")}-#{user.id}-#{Invoice.count + 1}"
  end
end
