module ReactProps
  class SkinCare
    class << self
      include Rails.application.routes.url_helpers
    end

    def self.props
      questionnaire_hash = {}
      questionnaire_hash.merge!({
        createQuestionnairePath: questionnaires_path,
        totalSteps: Question.count,
        12 => {
          column: "age",
          question: "How old are you?",
          textField: true,
          lastStep: true
        },
      })

      Question.includes(:answers).each do |qu|
          next if qu.answers.blank?
          props = { qu.question_number => {
                                            question: qu.text,
                                            column: qu.column_name
                                          } }

          questionnaire_hash.merge!(props)
          answer_hash = {answers: {}}

          qu.answers.pluck(:text).each_with_index do |text, index|
            answer_hash[:answers][index + 1] = text
          end
          questionnaire_hash[qu.question_number].merge!(answer_hash)
      end

      questionnaire_hash[1].merge!({ multipleAnswers: true })
      questionnaire_hash[2].merge!({ multipleAnswers: true })

      questionnaire_hash.to_json
    end
  end
end