module ReactProps
  class OrderSelection
    class << self
      include Rails.application.routes.url_helpers
    end

    def self.props
      {
        oneMonth: {
          strikePrice: "$148.00",
          subtotal: "$79.00",
          tax: "$7.30",
          shipping: "Free",
          total: "$86.30",
          amount: 8630,
          description: "One Month’s Sample -- Includes 1 Virtual Face-to- Face Consultation with Your Very Own Cosmetic Chemist"
          },
        threeMonth: {
          strikePrice: "$294.00",
          subtotal: "$147.00",
          tax: "$13.60",
          shipping: "Free",
          total: "$160.60",
          amount: 16060,
          description: "Quarterly Subscription -- Includes 4 Virtual Face-to- Face Consultations with Your Very Own Cosmetic Chemist"
        },
        logoUrl: ActionController::Base.helpers.image_url("logo.png")
      }.to_json
    end
  end
end