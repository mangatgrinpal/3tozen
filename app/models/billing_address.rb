class BillingAddress < Address
  def self.stripe_params_mapping(params)
    {
      "first_name": params["billing_name"],
      "zipcode": params["billing_zip"],
      "address": params["billing_address_line1"],
      "city": params["billing_address_city"].titleize,
      "state": params["billing_address_state"]
    }
  end
end