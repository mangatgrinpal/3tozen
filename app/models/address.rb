class Address < ApplicationRecord
  belongs_to :user

  def self.stripe_params_mapping(params)
    {
      "first_name": params["shipping_name"],
      "zipcode": params["shipping_address_zip"],
      "address": params["shipping_address_line1"].titleize,
      "city": params["shipping_address_city"],
      "state": params["shipping_address_state"]
    }
  end
end