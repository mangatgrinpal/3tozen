class Answer < ApplicationRecord
  belongs_to :question, optional: true

  before_save :set_answer_number


  def set_answer_number
    self.answer_number = question.answers.count + 1
  end
end
