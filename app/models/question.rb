class Question < ApplicationRecord
  belongs_to :questionnaire, optional: true
  has_many :answers, dependent: :destroy

  default_scope { order(:question_number) }
end
