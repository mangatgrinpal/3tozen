class Message < ApplicationRecord
	validates :name, presence: true, length: { minimum: 3, maximum: 30}
	validates :email, presence: true, length: { minimum: 3, maximum: 30}
	validates :subject, presence: true, length: { minimum: 3, maximum: 30}
	validates :body, presence: true, length: { minimum: 3, maximum: 256}
end