import React from "react";
import ReactDOM from "react-dom";

class Main extends React.Component {
  constructor() {
    super()
    this.renderPrice = this.renderPrice.bind(this)
  }

  componentWillMount() {
    this.setState({
      price: this.props.orderSelection.oneMonth,
      amount: this.props.orderSelection.oneMonth.amount
    })
  }

  componentDidMount() {
    var self = this;
    var handler = StripeCheckout.configure({
      key: 'pk_test_inyAcMCMJeLw7AGkagvsnJrc',
      image: self.props.orderSelection.logoUrl,
      locale: 'auto',
      billingAddress:true,
      shippingAddress: true,
    })

    document.getElementsByClassName("stripe-button-el")[0].addEventListener('click', function(e) {
      // Open Checkout with further options:

      handler.open({
        name: "3toZen",
        description: "Personalized Cleanser & Daily Soothing Concentrate",
        zipCode: true,
        billingAddress:true,
        shippingAddress: true,
        amount: self.state.amount,
        token: function(token, address) {
          // You can access the token ID with `token.id`.
          // Get the token ID to your server-side code for use.
          self.createHiddenAddressFields(address)
          document.querySelector("input[name=stripe_token]").value = token.id
          document.querySelector("input[name=stripe_email]").value = token.email
          document.querySelector("form").submit()
        }
      });
      e.preventDefault();
    });

    window.addEventListener('popstate', function() {
      handler.close();
    });

  }

  createHiddenAddressFields(address) {
    let addresses = [ "billing_name", "billing_address_country", "billing_address_country_code",
      "billing_address_zip", "billing_address_line1", "billing_address_city",
      "billing_address_state", "shipping_name", "shipping_address_country",
      "shipping_address_country_code", "shipping_address_zip", "shipping_address_line1",
      "shipping_address_city", "shipping_address_state"]

    addresses.forEach((el) => {
      let hidden = document.createElement("input")
      hidden.type = "hidden"
      hidden.name = el
      hidden.value = address[el]
      document.querySelector("form").append(hidden)
    })
  }

  renderPrice(e) {
    let value = e.target.value
    this.setState({
      price: this.props.orderSelection[value],
      amount: this.props.orderSelection[value].amount
    })
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-6 offset-md-3">
          <br/>
          <br/>

          <div className="cost centered">
            <h5>
              <strike>{this.state.price.strikePrice}</strike> {this.state.price.subtotal}
            </h5>

            <p>Pre-Launch Special</p>
            <br/>
            <p>Gentle Cleanser + Daily Soothing Concentrate + Virtual Face-to-Face Consultation with Your Very Own Cosmetic Chemist</p>
            <p><strong>{this.state.price.description}</strong></p>
            <br/>
          </div>

          <input type="radio" id="oneMonth" onClick={this.renderPrice} name="item" defaultChecked="true" value="oneMonth"/>&nbsp;
          <label htmlFor="oneMonth">One Month Sample</label>


          <br/>

          <input type="radio" onClick={this.renderPrice} id="threeMonth" name="item" value="threeMonth"/>&nbsp;
          <label htmlFor="threeMonth">Three Month - Quarterly Subscription</label>
          <br/>
          <small>Your newly personalized products will be shipped out quarterly. You can cancel at any time by emailing sopiea@3toZEN.com.</small>

          <input type="hidden" name="stripe_email" />
          <input type="hidden" name="stripe_token" />



          <hr/>

          <div className="row">
            <div className="col-md-8">
              Subotal:
            </div>
            <div className="col-md-4 right-aligned">
              {this.state.price.subtotal}
            </div>
          </div>

          <br/>

          <div className="row">
            <div className="col-md-8">
              Tax:
            </div>
            <div className="col-md-4 right-aligned">
              {this.state.price.tax}
            </div>
          </div>
          <br/>
          <div className="row">
            <div className="col-md-8">
              Shipping:
            </div>
            <div className="col-md-4 right-aligned">
              Free
            </div>
          </div>

          <br/>
          <hr/>

          <div className="row">
            <div className="col-md-8">
              Total:
            </div>
            <div className="col-md-4 right-aligned">
              {this.state.price.total}
            </div>
          </div>

          <br/>

          <div className="row centered">
            <div className="col-md-12">
              <button type="submit" className="stripe-button-el stripe-payment-button ">
                <span>Pay With Card</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const node = document.getElementById("order-selection-react")
const data = JSON.parse(node.getAttribute("data"))

ReactDOM.render(<Main orderSelection={data}/>, node.appendChild(document.createElement('div')))



