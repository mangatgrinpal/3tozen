import React from 'react'
import ReactDOM from 'react-dom'

class Questionnaire extends React.Component {
  constructor() {
    super()
    this.state = {}
    this.handleOnClickAnswer = this.handleOnClickAnswer.bind(this);
  }

  componentWillMount() {
    this.setStateOnMount()
  }

  setStateOnMount() {
    if (this.props.questionnaire[this.props.step]["answers"]) {
      let answersState = {}
      Object.values(this.props.questionnaire[this.props.step]["answers"]).forEach((e) => {
        answersState[e] = false
      })
      this.setState({answersState: answersState})
    }
  }

  renderAnswers() {
    if (this.isInputField()) {
      return this.renderInputField()
    } else {
      return this.renderAnswerChoices()
    }
  }

  renderAnswerChoices() {
    const answersSet = this.props.questionnaire[this.props.step]["answers"]
    const answers = Object.values(answersSet)

    return answers.map((answer, i) => {
      return (
        <div className="col-md-12" key={i + answer}>
          <p className={"selection " + this.handleHighlight(answer)} value={this.handleAnswerValue(answer)} onClick={this.handleOnClickAnswer}>
           {answer}
          </p>
        </div>
      )
    })
  }

  handleAnswerValue(answer) {
    if (answer === "Yes") {
      return true
    } else if ( answer === "No") {
      return false
    } else {
      return answer
    }
  }

  handleHighlight(answer) {
    return this.state.answersState[answer] == true ? "active" : ""
  }

  handleOnClickAnswer(e) {
    if (this.props.questionnaire[this.props.step]["multipleAnswers"]) {
      this.toggleClickedAnswerHighlight(e)
      this.props.saveAnswerToState(e)
    } else {
      this.props.nextStep(e)
    }
  }

  toggleClickedAnswerHighlight(e) {
    let newAnswersState = this.state.answersState
    let highlight = newAnswersState[e.target.getAttribute("value")] === true ? false : true

    newAnswersState[e.target.getAttribute("value")] = highlight
    this.setState({answersState: newAnswersState })
  }

  renderInputField() {
    return (
      <div className="col-md-12">
        <br/>
        <input type="text" key={this.props.step} onChange={this.props.saveAnswerToState} placeholder={this.props.column}/>
      </div>
    )
  }

  isInputField() {
    return this.props.questionnaire[this.props.step]["textField"] == true
  }

  render() {
    return (
      <div className="row">
        <div className="col-md-6">
          <br/>
          <div className="col-md-11 offset-md-1 question">
            <p >{this.props.questionnaire[this.props.step]["question"]}</p>
          </div>
        </div>
        <div className="col-md-6">
          {this.renderAnswers()}
        </div>
      </div>
    )
  }
}

export default Questionnaire