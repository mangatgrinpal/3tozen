// Run this example by adding <%= javascript_pack_tag 'hello_react' %> to the head of your layout file,
// like app/views/layouts/application.html.erb. All it does is render <div>Hello React</div> at the bottom
// of the page.

import React from 'react'
import ReactDOM from 'react-dom'
import Questionnaire from './questionnaire'
// PropTypes is for type checking. Catching bugs or errors.
import PropTypes from 'prop-types'

class Main extends React.Component {
  constructor() {
    super()
    this.state = { step: 1, skin_concern: [], skin_issue: [] }
    this.nextStep = this.nextStep.bind(this);
    this.previousStep = this.previousStep.bind(this);
    this.saveAnswerToState = this.saveAnswerToState.bind(this);
  }

  submitQuestionnaire() {
    $.ajax({
      type: "POST",
      url: this.props.questionnaire.createQuestionnairePath,
      data: {answers: this.formatData(this.state)},
      dataType: "json",
    })
    .done((response) => {
      window.location.href = response.redirectPath
    })
  }

  formatData() {
    let newState = {...this.state}
    newState["skin_concern"] = newState["skin_concern"].join(", ")
    newState["skin_issue"] = newState["skin_issue"].join(", ")
    return newState
  }

  nextStep(e) {
    var self = this;
    if (this.props.questionnaire[this.state.step].lastStep)  {
      this.submitQuestionnaire()
    } else {
      this.saveAnswer(e)
    }
  }

  saveAnswer(e) {
    // for some reason saveAnswerToState saves empty value when you click the next button for textfields
    // in saveAnswerToState maybe you need to clone the state object, then set state to that object, otherwise setting state to this.state doesn't save new state data.
    if (!this.props.questionnaire[this.state.step]["textField"] && !this.props.questionnaire[this.state.step]["multipleAnswers"]) { this.saveAnswerToState(e) }
    this.setState({step: this.state.step += 1})
  }

  saveAnswerToState(e) {
    const column = this.props.questionnaire[this.state.step].column
    const value = this.getValueFromElement(e)
    let newState = {...this.state}

    if (this.props.questionnaire[this.state.step]["multipleAnswers"]) {
      this.saveMultipleAnswers(value, column)
    } else {
      newState[column] = value
      this.setState(newState)
    }
  }

  saveMultipleAnswers(value, column) {
    let newState = {...this.state}
    newState[column] = this.updateAndReturnStateArrayElement(value, column)
    this.setState(newState)
  }

  isAnswerAlreadySelected(value, column) {
    for(let i=0; i < this.state[column].length; i++) {
      if (this.state[column][i] === value) { return true }
    }
    return false
  }

  updateAndReturnStateArrayElement(value, column) {
    var newStateArray = this.state[column]
    if (this.isAnswerAlreadySelected(value, column)) {
      newStateArray = this.state[column].filter((e) => {
        return e != value
      })
    } else {
      newStateArray.push(value)
    }
    return newStateArray
  }

  getValueFromElement(e) {
    return e.target.getAttribute("value") ? e.target.getAttribute("value") : e.target.value
  }

  previousStep() {
    this.setState({step: this.state.step - 1})
  }

  renderNextStepButton() {
    if (this.showNextStep()) {
      return <button className="btn btn-primary" onClick={this.nextStep}>Next</button>
    }
  }

  showNextStep() {
    return (this.props.questionnaire[this.state.step]["textField"] || this.props.questionnaire[this.state.step]["multipleAnswers"])
  }

  renderPreviousStep() {
    if (this.state.step > 1) {
      return <button className="btn btn-secondary" onClick={this.previousStep}>Back</button>
    }
  }

  render() {
    return (
      <div>
        <div className="modal-header">
          <h5>Personalize Your Products</h5>
          <span className="count">{this.state.step}/{this.props.questionnaire.totalSteps}</span>
          <button className="close" data-dismiss="modal"><span>&times;</span></button>
        </div>
        <div className="modal-body">
          <div className="row">
            <div className="col-md-12">
            <Questionnaire
              questionnaire={this.props.questionnaire}
              step={this.state.step}
              nextStep={this.nextStep}
              saveAnswerToState={this.saveAnswerToState}
              column={this.props.questionnaire[this.state.step].column}
              />
            </div>
          </div>
        </div>
        <div className="modal-footer">
          {this.renderPreviousStep()}
          {this.renderNextStepButton()}
        </div>
      </div>
    )
  }
}

Main.propTypes = {
  skin_concern: PropTypes.array,
  skin_issue: PropTypes.array,
  cleanser_wash_rate: PropTypes.string,
  exfoliate_rate: PropTypes.string,
  suncreen_childhood_daily: PropTypes.bool,
  sunscreen_daily: PropTypes.bool,
  hours_of_sleep: PropTypes.string,
  stress_level: PropTypes.string,
  color_of_urine: PropTypes.string,
  seasonal_allergies: PropTypes.bool,
  age: PropTypes.number,
  zipcode: PropTypes.string,
  face:PropTypes.string
}

// Turbolinks dooesn't page refresh
const node = document.getElementById('skincare-wizard-react')
const data = JSON.parse(node.getAttribute('data'))

ReactDOM.render(<Main questionnaire={data}/>, node.appendChild(document.createElement('div')))

