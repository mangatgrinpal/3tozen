class AddColumnsToQuestionnaire < ActiveRecord::Migration[5.1]
  def change
    add_column :questionnaires, :skin_concern, :string
    add_column :questionnaires, :cleanser_wash_rate, :string
    add_column :questionnaires, :exfoliate_rate, :string
    add_column :questionnaires, :suncreen_childhood_daily, :boolean
    add_column :questionnaires, :sunscreen_daily, :boolean
    add_column :questionnaires, :hours_of_sleep, :string
    add_column :questionnaires, :stress_level, :string
    add_column :questionnaires, :color_of_urine, :string
    add_column :questionnaires, :three_meals_a_day, :boolean
    add_column :questionnaires, :smoke, :boolean
    add_column :questionnaires, :exercise_per_week, :string
    add_column :questionnaires, :alcohol_per_week, :string
    add_column :questionnaires, :seasonal_allergies, :boolean
    add_column :questionnaires, :season_best_skin, :string
    add_column :questionnaires, :season_worst, :string
    add_column :questionnaires, :age, :integer
    add_column :questionnaires, :zipcode, :string
    add_column :questionnaires, :skin_image, :string
  end
end
