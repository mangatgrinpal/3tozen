class AssociateInvoiceToUser < ActiveRecord::Migration[5.1]
  def change
    add_reference :invoices, :user
  end
end
