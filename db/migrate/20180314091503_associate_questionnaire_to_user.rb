class AssociateQuestionnaireToUser < ActiveRecord::Migration[5.1]
  def change
  add_reference(:questionnaires, :user, index: true)
  end
end
