class RemoveColumnsFromQuestionnaire < ActiveRecord::Migration[5.1]
  def change
    add_column :questionnaires, :skin_issue, :string
    # typo fix
    remove_column :questionnaires, :suncreen_childhood_daily
    add_column :questionnaires, :sunscreen_childhood_daily, :boolean
    remove_column :questionnaires, :three_meals_a_day, :boolean
    remove_column :questionnaires, :smoke, :boolean
    remove_column :questionnaires, :exercise_per_week, :string
    remove_column :questionnaires, :alcohol_per_week, :string
    remove_column :questionnaires, :season_best_skin, :string
    remove_column :questionnaires, :season_worst, :string
  end
end

