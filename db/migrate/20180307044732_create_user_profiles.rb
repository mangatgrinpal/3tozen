class CreateUserProfiles < ActiveRecord::Migration[5.1]
  def change
    create_table :user_profiles do |t|
      t.string :first_name
      t.string :last_name
      t.string :address
      t.string :address_optional
      t.string :city
      t.string :state
      t.string :zipcode
      t.timestamps
    end
  end
end
