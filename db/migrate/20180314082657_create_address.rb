class CreateAddress < ActiveRecord::Migration[5.1]
  def change
    create_table :addresses do |t|
    t.string :first_name
    t.string :last_name
    t.string :address
    t.string :address_optional
    t.string :city
    t.string :state
    t.string :zipcode
    t.string :type
    t.references :user
  end
  end
end
