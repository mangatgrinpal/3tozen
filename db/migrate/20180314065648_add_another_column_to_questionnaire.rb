class AddAnotherColumnToQuestionnaire < ActiveRecord::Migration[5.1]
  def change
    add_column :questionnaires, :eye_symptoms, :boolean
  end
end