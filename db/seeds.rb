# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

questionnaire = Questionnaire.create(name: "Skincare")

q = Question.create(text: "What is/are your main skin concern(s). Please check all that applies.", question_number: 1, column_name: "skin_concern")

["Dull Skin",
"Dry Skin",
"Oily Skin",
"Combination Skin",
"Sensitive Skin",
"Hyperpigmentation",
"Age Line"].each do |a|
  Answer.create(text: a, question: q)
end

q = Question.create(text: "Do you have any of the following? Please check all that applies.", question_number: 2, column_name: "skin_issue")

["Acne",
"Rosacea",
"Eczema",
"Psoriasis",
"Redness"].each do |a|
  Answer.create(text: a, question: q)
end

q = Question.create(text: "Are you experiencing any of the following eye symptoms - redness, dryness, and/or gritty feeling?", question_number: 3, column_name: "eye_symptoms")

["Yes", "No"].each do |a|
  Answer.create(text: a, question: q)
end

q = Question.create(text: "Do you experience seasonal allergies?", question_number: 4, column_name: "seasonal_allergies")

["No", "Usually in the Spring", "Usually in the Summer", "Usually in the Fall", "Usually in the Winter" ].each do |a|
  Answer.create(text: a, question: q)
end

q = Question.create(text: "In a 24-hour period, how often do you wash your face with a cleanser?", question_number: 5, column_name: "cleanser_wash_rate")

["1x", "2x", "3x"].each do |a|
  Answer.create(text: a, question: q)
end

q = Question.create(text: "How often do you exfoliate your skin?", question_number: 6, column_name: "exfoliate_rate")

["Every Day", "1x/week", "1-2x/month", "Never"].each do |a|
  Answer.create(text: a, question: q)
end

q = Question.create(text: "Growing up, was sunscreen a part of your daily regimen?", question_number: 7, column_name: "sunscreen_childhood_daily")

["Yes", "No"].each do |a|
  Answer.create(text: a, question: q)
end

q = Question.create(text: "Do you apply sunscreen daily?", question_number: 8, column_name: "sunscreen_daily")

["Yes", "No"].each do |a|
  Answer.create(text: a, question: q)
end

q = Question.create(text: "How many hours do you sleep?", question_number: 9, column_name: "hours_of_sleep")

["Less than 6-8 hours", "6-8 Hours", "More than 6-8 hours"].each do |a|
  Answer.create(text: a, question: q)
end

q = Question.create(text: "What is your daily stress level?", question_number: 10, column_name: "stress_level")

["Low", "Medium", "High"].each do |a|
  Answer.create(text: a, question: q)
end

q = Question.create(text: "What is the color of your urine?", question_number: 11, column_name: "color_of_urine")

["Don't Know", "Colorless", "Pale Straw Color", "Yellow", "Dark Yellow"].each do |a|
  Answer.create(text: a, question: q)
end

q = Question.create(text: "How old are you?", question_number: 12, column_name: "age", field_type: "text_field")

